require('./bootstrap');

window.Vue = require('vue').default;
let Vue = window.Vue;

// packages
import router from './router';
import 'es6-promise/auto';
import store from './store';
import {BootstrapVue, IconsPlugin} from "bootstrap-vue";
import Lang from "lang.js";
import helpers from "./helpers";

// init packages
Vue.use(BootstrapVue);
Vue.use(IconsPlugin);

// locales
import locales from "./messages";
const messages = locales;
Vue.prototype.trans = new Lang({
    messages,
    locale: localStorage.getItem('locale') ?? process.env.DEFAULT_LOCALE,
    fallback: process.env.FALLBACK_LOCALE
});

// helpers
Vue.prototype.$helpers = helpers;

// css, js
import '../../public/css/app.css'

// components
import App from './components/layouts/App';

// bootstrap app
const app = new Vue({
    router,
    el: '#app',
    store: store,
    render: h => h(App)
});