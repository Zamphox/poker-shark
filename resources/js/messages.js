export default {
    'en.general': {
        featured_game_title: 'Next game',
        viber_group_title: 'Viber group',
        location: 'Location',
        password: 'Password',
        email: 'Email',
        login: 'Login',
        sign_up: 'Sign up',
        forgot_password: 'Forgot password?',
        logout: 'Logout'
    },
    'en.validation': {
        required_email_max_length: 'Field is required, must be an email and not longer then :length characters',
        required_min_length: 'Field is required and must be longer then :length characters'
    },
    'en.errors': {
        unprocessable: 'Invalid credentials',
        server_error: 'A server error has occurred',
    },



    'ru.general': {
        featured_game_title: 'Следующая игра',
        viber_group_title: 'Viber группа',
        location: 'Локация',
        email: 'Имэйл',
        password: 'Пароль',
        login: 'Логин',
        sign_up: 'Зарегистрироваться',
        forgot_password: 'Забыли пароль?',
        logout: 'Выйти'
    },
    'ru.validation': {
        required_email_max_length: 'Поле должно быть заполнено в формате email, и не быть длиннее чем :length characters символов',
        required_min_length: 'Поле должно быть заполнено и быть длиннее чем :length characters символов'
    },
    'ru.errors': {
        unprocessable: 'Неверный логин/пароль',
        server_error: 'Произошла ошибка на сервере',
    },
}