export default {
    validateState: function(validator, name) {
        const { $dirty, $error } = validator[name].value;

        return $dirty ? !$error : null;
    },

    formAjaxErrorResponseMessageKey: function (error) {
        if(error.response.status === 422){
            return  'errors.unprocessable';
        } else {
            return 'errors.server_error';
        }
    }
}