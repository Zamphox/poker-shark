import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex);

const store = new Vuex.Store({
    state: {
        isAuthenticated: false,
    },
    mutations: {
        authenticate: function (state, token) {
            if(token){
                state.isAuthenticated = true;
                axios.defaults.headers.common['Authorization'] = 'Bearer ' + token;
                localStorage.setItem('_token', token);
            }
        },
        deauthenticate: function () {
            this.state.isAuthenticated = false;
            localStorage.removeItem('_token');
        }
    },
    getters: {
        authenticated: function (state) {
            return state.isAuthenticated;
        }
    }
});

export default store;