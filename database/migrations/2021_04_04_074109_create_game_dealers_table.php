<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGameDealersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('game_dealers', function (Blueprint $table) {
            $table->unsignedBigInteger('game_id');
            $table->unsignedBigInteger('dealer_id');

            $table->foreign('game_id')->references('id')->on('games');
            $table->foreign('dealer_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('game_dealers', function (Blueprint $table){
            $table->dropForeign('game_dealers_game_id_foreign');
            $table->dropForeign('game_dealers_dealer_id_foreign');
        });
        Schema::dropIfExists('game_dealers');
    }
}
