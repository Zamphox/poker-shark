<?php

namespace Database\Factories;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class UserFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = User::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->name,
            'email' => $this->faker->unique()->safeEmail,
            'password' => Hash::make(env('DEFAULT_ADMIN_PASSWORD')),
        ];
    }

    public function createAdminUser()
    {
        $admin = new $this->model;
        $admin->name = 'Admin';
        $admin->email = env('DEFAULT_ADMIN_EMAIL');
        $admin->password = Hash::make(env('DEFAULT_ADMIN_PASSWORD'));
        $admin->avatar = Config::get('cnst.default_logo_path');
        $admin->role_id = Config::get('cnst.roles.admin');

        $admin->save();
    }

    /**
     * Guest user needs to be created from the start,
     * because it will be used for accepting unregistered users into the game
     */
    public function createGuestUser()
    {
        $guest = new $this->model;
        $guest->name = 'Guest';
        $guest->email = 'notanemail@m.com';
        $guest->password = '';
        $guest->role_id = Config::get('cnst.roles.guest');

        $guest->save();
    }

    public function createDealerUsers()
    {
        $this->count(3)->create([
                                       'role_id' => Config::get('cnst.roles.dealer')
                                   ]);
    }

    public function createPlayerUsers()
    {
        $this->count(8)->create([
                                       'role_id' => Config::get('cnst.roles.player')
                                   ]);
    }
}
