<?php

namespace Database\Factories;

use App\Models\Game;
use App\Models\User;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Facades\DB;

class GamesFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Game::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
            'name' => $this->faker->randomElement(['Knockout', 'Re-Buy', 'Crazy Re-Buy', '5/5', '5/10']),
            'start_at' => $this->faker->dateTimeBetween('-' . $this->faker->numberBetween(1, 14)
                            . ' days')->setTime($this->faker->numberBetween(18, 20), 0),
            'description' => $this->faker->numberBetween(1, 3) . '00/'
                . $this->faker->numberBetween(3,5) . '00/'
                . $this->faker->numberBetween(5,8) . '00 everyone who comes before '
                . $this->faker->numberBetween(18, 20) . ':00 gets +'
                . $this->faker->numberBetween(2, 5) . '000 additional chips',
            'prize_fund' => '1 - ' . $this->faker->randomElement(['20', '30', '40']) . '00,' .
                ' 2 - ' . $this->faker->randomElement(['10', '20', '20']) . '00,' .
                ' 3 - ' . $this->faker->randomElement(['30', '40', '60', '80']) . '0'
        ];
    }

    /**
     *
     */
    public function createGames()
    {
        $games = $this->count(16)->create();
        $this->populateGamesWithPlayersAndDealers($games);
    }

    /**
     * @param Collection $games
     */
    private function populateGamesWithPlayersAndDealers(Collection $games)
    {
        $players = User::select(['id', 'role_id'])->get()->keyBy('id');
        $dealers = $players->filter(function ($player) {
            return $player->isADealer();
        });
        $playerIdKeys = $players->keys();
        $dealerIdKeys = $dealers->keys();
        $gamePlayersInsert = [];
        $gameDealersInsert = [];

        foreach ($games as $game){
            for ($x = 1; $x <= $this->faker->numberBetween(4,12); $x++) {
                $playerId = $this->faker->numberBetween($playerIdKeys->first(), $playerIdKeys->last());
                if(empty($gamePlayersInsert[$game->id][$playerId])){
                    $gamePlayersInsert[$game->id][$playerId] = [
                        'game_id' => $game->id,
                        'user_id' => $playerId
                    ];
                }
            }

            for ($x = 1; $x <= $this->faker->numberBetween(1,2); $x++) {
                $dealerId = $this->faker->numberBetween($dealerIdKeys->first(), $dealerIdKeys->last());
                if(empty($gameDealersInsert[$game->id][$dealerId])){
                    $gameDealersInsert[$game->id][$dealerId] = [
                        'game_id' => $game->id,
                        'dealer_id' => $dealerId
                    ];
                }
            }
        }
        $gamePlayersInsert = $this->reArrangeInsertArray($gamePlayersInsert);
        $gameDealersInsert = $this->reArrangeInsertArray($gameDealersInsert);

        DB::table('game_players')->insert(array_values($gamePlayersInsert));
        DB::table('game_dealers')->insert(array_values($gameDealersInsert));
    }

    /**
     * @param $insert
     * @return array
     */
    private function reArrangeInsertArray($insert): array {
        $result = [];

        foreach ($insert as $gameEntry){
            foreach ($gameEntry as $entry){
                $result[] = $entry;
            }
        }

        return $result;
    }
}
