<?php

namespace Database\Seeders;

use Database\Factories\GamesFactory;
use Database\Factories\UserFactory;
use Faker\Factory;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    protected $faker;
    protected $userFactory;
    protected $gameFactory;

    public function __construct()
    {
        $this->faker = Factory::create();
        $this->userFactory = new UserFactory();
        $this->gameFactory = new GamesFactory();
    }

    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->userFactory->createAdminUser();
        $this->userFactory->createGuestUser();
        $this->userFactory->createDealerUsers();
        $this->userFactory->createPlayerUsers();
        $this->gameFactory->createGames();
    }
}
