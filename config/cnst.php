<?php

return [
    'roles' => [
        'guest' => 0,
        'admin' => 1,
        'dealer' => 2,
        'player' => 3
    ],
    'default_logo_path' => '/img/logo.png'
];
