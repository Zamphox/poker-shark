<?php

namespace App\Providers;

use App\Services\AuthenticationService\AuthenticationService;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Laravel\Passport\Passport;
use App\Services\AuthenticationService\AuthenticationServiceInterface;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Models\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();
        Passport::routes();
        //
    }

    public function register()
    {
        $this->app->bind(AuthenticationServiceInterface::class, function (){
            return new AuthenticationService();
        });
    }
}
