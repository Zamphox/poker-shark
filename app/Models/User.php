<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Query\Builder;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Config;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function scopeDealers(Builder $query){
        $query->where('role_id', Config::get('cnst.roles.dealer'));
    }

    public function scopePlayersAndGuests(Builder $query){
        $query->whereIn('role_id', [Config::get('cnst.roles.player'), Config::get('cnst.roles.guest')]);
    }

    /**
     * @return bool
     */
    public function isADealer(): bool
    {
        return $this->role_id === Config::get('cnst.roles.dealer');
    }
}
