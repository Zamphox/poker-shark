<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Game extends Model
{
    use HasFactory;

    /**
     * @return BelongsToMany
     */
    public function dealers(): BelongsToMany
    {
        return $this->belongsToMany(User::class, 'game_dealers', 'game_id', 'dealer_id');
    }

    /**
     * @return BelongsToMany
     */
    public function players(): BelongsToMany
    {
        return $this->belongsToMany(User::class, 'game_players', 'game_id', 'user_id');
    }
}
