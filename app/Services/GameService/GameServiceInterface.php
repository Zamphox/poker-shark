<?php


namespace App\Services\GameService;


use Illuminate\Contracts\Pagination\Paginator;

interface GameServiceInterface
{
    /**
     * @return Paginator
     */
    public function getPaginatedGamesListCollection(): Paginator;
}