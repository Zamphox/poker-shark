<?php

namespace App\Services\GameService;


use App\Models\Game;
use Illuminate\Contracts\Pagination\Paginator;

class GameService implements GameServiceInterface
{
    private $perPage;

    public function __construct($perPage = 8)
    {
        $this->perPage = $perPage;
    }

    /**
     * @return Paginator
     */
    public function getPaginatedGamesListCollection(): Paginator
    {
        return Game::with(['players', 'dealers'])->orderBy('start_at', 'desc')->simplePaginate($this->perPage);
    }
}