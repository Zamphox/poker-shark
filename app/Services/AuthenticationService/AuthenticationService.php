<?php

namespace App\Services\AuthenticationService;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthenticationService implements AuthenticationServiceInterface
{
    /**
     * @param array $credentials
     * @return JsonResponse
     * @throws \Exception
     */
    public function loginUser(array $credentials): JsonResponse
    {
        if (Auth::attempt($credentials)) {
            $user = Auth::user();
            $token = $user->createToken('Personal Access Token')->accessToken;
            $cookie = $this->getCookieDetails($token);

            return response()
                ->json(
                    [
                        'success' => 1,
                        'user' => $user,
                        'token' => $token,
                    ]
                )
                ->cookie(
                    $cookie['name'],
                    $cookie['value'],
                    $cookie['minutes'],
                    $cookie['path'],
                    $cookie['domain'],
                    $cookie['secure'],
                    $cookie['httponly'],
                    $cookie['samesite']
                );
        } else {
            throw new \Exception('Invalid credentials');
        }
    }

    /**
     * @param string $token
     * @return array
     */
    public function getCookieDetails(string $token): array
    {
        return [
            'name' => '_token',
            'value' => $token,
            'minutes' => env('AUTH_TOKEN_LIFETIME') ?? 10080,
            'path' => null,
            'domain' => null,
            'secure' => env('APP_ENV') !== 'local',
            'httponly' => true,
            'samesite' => true,
        ];
    }

    /**
     * @param Request $request
     * @return string[]
     */
    public function logoutUser(Request $request): array
    {
        $request->user()->token()->revoke();

        return [
            'message' => 'Logged out'
        ];
    }
}
