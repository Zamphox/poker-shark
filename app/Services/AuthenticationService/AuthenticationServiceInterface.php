<?php
namespace App\Services\AuthenticationService;

use Illuminate\Http\Request;

interface AuthenticationServiceInterface
{
    /**
     * @param array $credentials
     * @return mixed
     */
    public function loginUser(array $credentials);

    /**
     * @param string $token
     * @return mixed
     */
    public function getCookieDetails(string $token);

    /**
     * @param Request $request
     * @return mixed
     */
    public function logoutUser(Request $request);
}
