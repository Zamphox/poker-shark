<?php

namespace App\Http\Controllers;

use App\Models\Game;
use App\Services\GameService\GameService;
use Illuminate\Http\JsonResponse;

class GamesController extends Controller
{
    protected $gameService;

    public function __construct(GameService $gameService)
    {
        $this->gameService = $gameService;
    }

    /**
     * @return JsonResponse
     */
    public function getPaginatedGamesList(): JsonResponse
    {
        return $this->returnJsonSuccess($this->gameService->getPaginatedGamesListCollection()->toArray());
    }
}
