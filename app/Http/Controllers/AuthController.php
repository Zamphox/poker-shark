<?php

namespace App\Http\Controllers;

use App\Services\AuthenticationService\AuthenticationService;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class AuthController extends Controller
{
    use AuthenticatesUsers;

    private $authService;

    public function __construct()
    {
        $this->middleware('guest')->except('logout');
        $this->authService = new AuthenticationService();
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function login(Request $request): JsonResponse
    {
        try {
            $request->validate(
                [
                    'email' => 'required|string|email',
                    'password' => 'required|string|min:8',
                ]
            );

            return $this->authService->loginUser(request(['email', 'password']));
        } catch (\Exception $exception) {
            return $this->returnJsonError(isset($exception->validator) ? $exception->validator->errors()->first() : $exception->getMessage());
        }
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function logout(Request $request): JsonResponse
    {
        return $this->returnJsonSuccess($this->authService->logoutUser($request));
    }
}