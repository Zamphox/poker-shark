<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Validation\ValidationException;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /**
     * @param string $errorMessage
     * @param null $errorLangKey
     * @return \Illuminate\Http\JsonResponse
     */
    public function returnJsonError(string $errorMessage, $errorLangKey = null){
        return response()->json(
            [
                'success' => 0,
                'error' => $errorMessage,
                'error_lang_key' => $errorLangKey
            ], 422
        );
    }

    /**
     * @param array $data
     * @return \Illuminate\Http\JsonResponse
     */
    public function returnJsonSuccess(array $data){
        $responseArray = array_merge($data, [
            'success' => 1
        ]);
        return response()->json($responseArray);
    }
}
